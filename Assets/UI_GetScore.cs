﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_GetScore : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<Text>().text = "SCORE: " + LevelManager.PointerLM.bestScore + " / " + LevelManager.PointerLM.score;
	}
}
