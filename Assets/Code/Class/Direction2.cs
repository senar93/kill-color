﻿
[System.Serializable]
public class Direction2 {

	public float x, y;


	public Direction2(float newX, float newY) {
		x = newX;
		y = newY;
	}


	public bool IsEqual(Direction2 coordToCompare) {
		if (x == coordToCompare.x && y == coordToCompare.y)
			return true;
		return false;
	}


	public Direction2Enum GetDirection() {
		if (x != 0) {                                               // x != 0
			if (y == 0) {                                               // x != 0 && y == 0
				if (x > 0)                                                  // x != 0 && y > 0
					return Direction2Enum.right;
				else                                                        // x != 0 y < 0
					return Direction2Enum.left;
			}
			else {                                                  // x == 0
				if (x > 0) {                                               // x > 0 && y != 0
					if (y > 0)                                                  // x > 0 && y > 0
						return Direction2Enum.up_right;
					else                                                        // x > 0 && y < 0
						return Direction2Enum.down_right;
				}
				else {                                                  // x < 0 && y != 0
					if (y > 0)                                                  // x < 0 && y > 0
						return Direction2Enum.up_left;
					else                                                        // x < 0 && y < 0
						return Direction2Enum.down_left;
				}
			}
		} else if (y != 0) {                                       // x == 0 && y != 0		
			if (y > 0)                                                  // x == 0 && y > 0
				return Direction2Enum.up;
			else                                                        // x == 0 && y < 0
				return Direction2Enum.down;
		} else {                                                  // x == 0 && y == 0
			return Direction2Enum.none;
		}
	}



}