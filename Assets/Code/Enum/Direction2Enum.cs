﻿public enum Direction2Enum {
	up = 0,
	up_right = 1,
	right = 2,
	down_right = 3,
	down = 4,
	down_left = 5,
	left = 6,
	up_left = 7,

	none = 8

}