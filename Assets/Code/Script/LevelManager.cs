﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	#region Pointer
	public static LevelManager PointerLM;
	public static DirectionManager PointerDM;
	public static EnemyManager PointerEM;
	public static Player PointerP;

	public Transform PointerBulletList;
	#endregion
	#region level parameter
	public string sceneName = "";
	public int startPlayerLife = 1;

	public float bulletSpeedMul = 1f;
	public float enemySpeedMul = 1f;
	public float gameSpeedMul = 1f;
	#endregion
	#region score, timer
	public int score = 0;
	public int bestScore;
	public Timer gameTime = new Timer(864000, 0);
	#endregion

	#region Start, Awake, Update
	// Use this for initialization
	void Start () {
		PointerLM = this;
		bestScore = PlayerPrefs.GetInt("Best Score " + sceneName);
		Time.timeScale = gameSpeedMul;
	}
	
	// Update is called once per frame
	void Update () {
		gameTime.Advance();
	}

	#endregion


	public void GameEnd() {
		if (score > bestScore)
			PlayerPrefs.SetInt("Best Score " + sceneName, score);
		SceneManager.LoadScene(sceneName);
	}


}
