﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public int life = 1;
	public Timer timeBetweenShoot = new Timer(0.1f, 0f);
	public Timer timeBetweenShootSameDirection = new Timer(0.25f, 0f);
	public GameObject bulletPrefab;

	[SerializeField] private Transform cannonPosition;
	private Direction2 directionToShoot = new Direction2(0, 0);
	private Direction2Enum oldDirection;


	#region Start, Update
	void Start () {
		LevelManager.PointerP = this;
		life = LevelManager.PointerLM.startPlayerLife;
	}
	

	void Update () {
		oldDirection = directionToShoot.GetDirection();
		directionToShoot.x = Input.GetAxis("Horizontal");
		directionToShoot.y = Input.GetAxis("Vertical");
		timeBetweenShoot.Advance();
		timeBetweenShootSameDirection.Advance();

		Direction2Enum tmpDirection = directionToShoot.GetDirection();
		if (tmpDirection != Direction2Enum.none && timeBetweenShoot.Check()) {
			if (tmpDirection != oldDirection || tmpDirection == oldDirection && timeBetweenShootSameDirection.Check()) {
				timeBetweenShoot.Reset();
				timeBetweenShootSameDirection.Reset();
				Shoot(tmpDirection);
			}
		}

	}

	#endregion


	private void Shoot(Direction2Enum tmpDirection) {
		int tmpIndex = LevelManager.PointerDM.GetDirectionIndex(tmpDirection);
		if (tmpIndex >= 0) {
			Vector3 newRotation = new Vector3(0f, 0f, DirectionManager.directionAngle[tmpIndex]);
			this.transform.localEulerAngles = newRotation;
			if (bulletPrefab != null) {
				GameObject bullet = (GameObject)Instantiate(bulletPrefab);
				bullet.transform.localEulerAngles = newRotation;
				bullet.GetComponent<Bullet>().objToReach = LevelManager.PointerDM.GetDirectionTransform(tmpIndex);
				bullet.transform.position = cannonPosition.position;
				bullet.transform.parent = LevelManager.PointerLM.PointerBulletList;
			}
		}

	}






}
