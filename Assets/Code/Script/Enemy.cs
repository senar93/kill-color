﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public EnemyType baseType;

	public float speed;
	public int life = 1;
	public int lifeAct;
	public int damage = 1;


	public Transform objToReach;

	void Start() {
		lifeAct = life;
	}

	// Update is called once per frame
	void Update()
	{
		if (objToReach == null)
			Destroy(this.gameObject);
		else {
			this.transform.position = Vector3.MoveTowards(this.transform.position,
														  objToReach.transform.position,
														  speed * LevelManager.PointerLM.enemySpeedMul * Time.deltaTime);
		}
	}



	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			if (!baseType.IsWrong) {
				Player player = other.GetComponent<Player>();
				player.life -= damage;
				if (player.life <= 0)
					LevelManager.PointerLM.GameEnd();
			} else {
				LevelManager.PointerLM.score += life;
			}
			Destroy(this.gameObject);
		}
	}


}
