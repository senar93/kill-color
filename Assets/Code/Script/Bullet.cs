﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public Transform objToReach;
	public float speed;
	public int damage = 1;

	
	// Update is called once per frame
	void Update () {
		if (objToReach == null)
			Destroy(this.gameObject);
		else {
			this.transform.position = Vector3.MoveTowards(this.transform.position,
														  objToReach.transform.position,
														  speed * LevelManager.PointerLM.bulletSpeedMul * Time.deltaTime);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Enemy") {
			Enemy enemyScript = other.gameObject.GetComponent<Enemy>();
			enemyScript.lifeAct -= damage;
			if (enemyScript.lifeAct <= 0) {
				if(!enemyScript.baseType.IsWrong)
					LevelManager.PointerLM.score += enemyScript.life;
				else {
					LevelManager.PointerLM.score -= enemyScript.life;
					LevelManager.PointerP.life -= enemyScript.life;
					if (LevelManager.PointerP.life <= 0)
						LevelManager.PointerLM.GameEnd();
				}
				Destroy(other.gameObject);
			}
			
			Destroy(this.gameObject);

		} else if (other.gameObject.tag == "Direction") {
			Destroy(this.gameObject);
		}
	}

}
