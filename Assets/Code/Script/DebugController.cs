﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour {

	public float dcgs = 0.1f;
	public float dcbs = 0.1f;
	public float dces = 0.1f;
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale + dcgs * Input.GetAxis("Debug cgs") > 0.1 && Time.timeScale + dcgs * Input.GetAxis("Debug cgs") < 100)
			Time.timeScale += dcgs * Input.GetAxis("Debug cgs");
		if(LevelManager.PointerLM.bulletSpeedMul + dcbs * Input.GetAxis("Debug cbs") > 0.1)
			LevelManager.PointerLM.bulletSpeedMul += dcbs * Input.GetAxis("Debug cbs");
		if (LevelManager.PointerLM.enemySpeedMul + dces * Input.GetAxis("Debug ces") > 0.1)
			LevelManager.PointerLM.enemySpeedMul += dces * Input.GetAxis("Debug ces");
		if (Input.GetButtonDown("Debug al"))
			this.GetComponent<Player>().life++;
		if (Input.GetButtonDown("Debug restart"))
			LevelManager.PointerLM.GameEnd();
	}
}
