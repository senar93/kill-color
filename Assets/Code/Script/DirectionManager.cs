﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionManager : MonoBehaviour {

	public static float[] directionAngle = { 0f, -45f, -90f, -135f, -180f, -225f, -270f, -315f };

	public float directionObjDistance = 4f;
	public int realDiretionCount;

	[SerializeField] private Direction2Enum[] directions;
	[SerializeField] private Transform[] directionObj;
	

	// Use this for initialization
	void Start () {
		LevelManager.PointerDM = this;
		SetTargetDistance(directionObjDistance);
		for (int i = 0; i < directions.Length; i++)
			if (directions[i] != Direction2Enum.none)
				realDiretionCount++;
	}
	

	public void SetTargetDistance(float newDistance) {
		directionObjDistance = newDistance;

		float j = 0;
		float angle;
		Vector3 newPos;

		for (int i = 0;
			i < directionObj.Length && directionObj[i] != null;
			i++, j += 0.125f)
		{
			angle = j * Mathf.PI * 2;
			newPos = new Vector3(Mathf.Sin(angle) * directionObjDistance, Mathf.Cos(angle) * directionObjDistance, 0);
			directionObj[i].position = newPos + transform.position;
		}
	}

	/// <summary>
	/// dato l'indice del array completo restituisce il puntatore a Transform dell oggetto corrispondente
	/// </summary>
	/// <param name="i"></param>
	/// <returns></returns>
	public Transform GetDirectionTransform(int i) {
		if (i >= 0 && i < directionObj.Length)
			return directionObj[i];
		return null;
	}
	
	/// <summary>
	/// data una direzione rerstituisce l'indice in cui si trova, se non c'è -1
	/// </summary>
	/// <param name="directionToCheck"></param>
	/// <returns></returns>
	public int GetDirectionIndex(Direction2Enum directionToCheck) {
		for (int i = 0; i < directions.Length; i++)
			if (directions[i] == directionToCheck)
				return i;
		return -1;
	}

	/// <summary>
	/// restituisce l'indice del array completo con tutte le direzioni,
	/// prendendo in input un indice che esclude le direzioni nulle
	/// </summary>
	/// <param name="fakeIndex"></param>
	/// <returns></returns>
	public int GetDirectionIndexByFakeIndex(int fakeIndex) {
		int count = 0;
		for(int i = 0; i < directions.Length; i++)
			if(directions[i] != Direction2Enum.none) {
				if (fakeIndex <= count) {
					return i;
				}
				count++;
			}
		return 0;
	}

}
