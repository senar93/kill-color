﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGameSpeedByTimeScript : MonoBehaviour {

	public float baseGameSpeed;
	public float gameSpeedIncXSec = 0.01f;

	void Start() {
		baseGameSpeed = LevelManager.PointerLM.gameSpeedMul;
	}

	// Update is called once per frame
	void Update () {
		float newGameSpeed = LevelManager.PointerLM.gameTime.GetActTime() * gameSpeedIncXSec + baseGameSpeed;
		LevelManager.PointerLM.gameSpeedMul = newGameSpeed;
		Time.timeScale = newGameSpeed;
	}


}
