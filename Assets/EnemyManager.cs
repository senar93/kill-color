﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

	public int enemyRightSpawnRate = 75;
	public int enemyWrongSpawnRate = 25;
	private int totalSpawnRateWrongRight;
	private int totalSpawnRateRight;
	private int totalSpawnRateWrong;

	public float minRandomTimeFromSpawn = 0.05f;
	public float maxRandomTimeFromSPawn = 0.3f;
	public Timer nextSpawnTimer = new Timer(4f, 0f);
	public float standardSpawnTimer = 0.3f;

	public GameObject enemyRightPrefabDefault;
	public GameObject enemyWrongPrefabDefault;

	public List<EnemyType> enemyRight = new List<EnemyType>();
	public List<EnemyType> enemyWrong = new List<EnemyType>();



	#region Start, Update
	// Use this for initialization
	void Start () {
		LevelManager.PointerEM = this;
		InitializeEnemyList();
	}
	

	// Update is called once per frame
	void Update () {
		nextSpawnTimer.Advance();
		if(nextSpawnTimer.Check()) {
			float extraSpawnTime = Random.Range(minRandomTimeFromSpawn, maxRandomTimeFromSPawn);
			nextSpawnTimer.SetMaxTime(standardSpawnTimer + extraSpawnTime);
			nextSpawnTimer.Reset();
			SpawnEnemy();
		}
	}

	#endregion


	#region Initialize Function
	public void InitializeEnemyList() {
		totalSpawnRateWrongRight = enemyRightSpawnRate + enemyWrongSpawnRate;
		totalSpawnRateRight = 0;
		totalSpawnRateWrong = 0;

		for (int i = 0; i < enemyRight.Count; i++) {
			if (enemyRight[i].prefab == null)
				enemyRight[i].prefab = enemyRightPrefabDefault;
			totalSpawnRateRight += enemyRight[i].spawnRate;
		}
		for (int i = 0; i < enemyWrong.Count; i++) {
			if (enemyWrong[i].prefab == null)
				enemyWrong[i].prefab = enemyWrongPrefabDefault;
			totalSpawnRateWrong += enemyWrong[i].spawnRate;
		}
	}

	#endregion


	private int GetIndexByRndSpawn(int spawnWeigh, List<EnemyType> enemyList) {
		for(int i = 0, count = 0; i < enemyList.Count; i++) {
			count += enemyList[i].spawnRate;
			if (count > spawnWeigh)
				return i;
		}

		return 0;
	}

	private void SpawnEnemy() {
		int rawPositionIndex = Random.Range(0, LevelManager.PointerDM.realDiretionCount);
		int tmp = Random.Range(0, totalSpawnRateWrongRight);
		int i;
		//calcolo right wrong
		if (tmp < enemyRightSpawnRate) {		//right
			i = GetIndexByRndSpawn(Random.Range(0, totalSpawnRateRight), enemyRight);
			OnlySpawnObj(enemyRight, i, rawPositionIndex);
		}
		else {																	//wrong
			i = GetIndexByRndSpawn(Random.Range(0, totalSpawnRateWrong), enemyWrong);
			OnlySpawnObj(enemyWrong, i, rawPositionIndex);
		}

	}

	private void OnlySpawnObj(List<EnemyType> sourceList, int specialIndex, int rawPositionIndex) {
		GameObject enemy = (GameObject)Instantiate(sourceList[specialIndex].prefab);
		int realIndex = LevelManager.PointerDM.GetDirectionIndexByFakeIndex(rawPositionIndex);
		enemy.transform.position = LevelManager.PointerDM.GetDirectionTransform(realIndex).position;
		enemy.transform.parent = this.transform;
		enemy.GetComponent<Enemy>().objToReach = LevelManager.PointerP.transform;
		//impostare la rotazione del nemico
		enemy.GetComponent<Enemy>().baseType = sourceList[specialIndex];
		if (sourceList[specialIndex].setCustomLife)
			enemy.GetComponent<Enemy>().life = sourceList[specialIndex].life;
		//enemy.GetComponent<Enemy>().lifeAct = enemyRight[i].life;
		if (sourceList[specialIndex].setCustomDamage)
			enemy.GetComponent<Enemy>().damage = sourceList[specialIndex].damage;
		if (sourceList[specialIndex].setCustomSpeed)
			enemy.GetComponent<Enemy>().speed = sourceList[specialIndex].speed;
		if (sourceList[specialIndex].setCustomColor)
			enemy.GetComponent<SpriteRenderer>().color = sourceList[specialIndex].color;
		if (sourceList[specialIndex].sprite != null)
			enemy.GetComponent<SpriteRenderer>().sprite = sourceList[specialIndex].sprite;
	}



}



[System.Serializable]
public class EnemyType {

	public bool IsWrong = false;
	public int spawnRate = 10;

	public bool setCustomLife = false;
	public int life = 1;
	public bool setCustomSpeed = false;
	public float speed = 6;
	public bool setCustomDamage = false;
	public int damage = 1;

	public bool setCustomColor = false;
	public Color color;
	public Sprite sprite;

	/// <summary>
	/// usato solo se != null, altrimenti viene settato a un valore di default
	/// </summary>
	public GameObject prefab = null;

	
}